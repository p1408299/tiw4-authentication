Rapport de la partie A
======================

### Mesures de sécurité et nouvelles fonctionnalités mises en place

| Description du problème | Mesure proposée et justifiée | Renvoi vers l'annexe |
| :-------------: | :---------------: | :-------------: |
| Accès au site en HTTP | Pour répondre à ce problème, nous avons décidé de mettre en place un protocole HTTPS (HyperText Transfer Protocol Secure). Il s’agit avant tout d’un protocole de communication client-serveur, qui bénéficie d'une couche de chiffrement SSL (Secure Socket Layer) pour ne pas compromettre la sécurité des informations échangées. La mise en place de ce protocole permettra par exemple de lutter contre les attaques du genre « Man In The Middle », lesquelles permettent à une entité tierce de récupérer les informations que vous échangez avec vos visiteurs. | cf. Annexe 1 |
| Paramétrage de la session d'un client | Afin d'accroître la sécurité de notre site nous allons paramétrer l'utilisation des sessions utilisateurs. Tout d'abord, nous allons limiter l'accès à la session à 5 minutes. Dépassé ce délai une nouvelle session sera générée. Dans un deuxième temps, nous avons décidé de ne pas autoriser les tickets de sessions. Cela permet de garantir la confidentialité des comunications passées même si la clé privée de chiffrement est compromise. De plus, nous avons mis en place l'autorisation de seulement quelques protocoles (Protocole TLS). Le protocole TLS est une des solutions les plus répandues pour la protection des flux réseaux. Pour finir les suites de chiffrements permettent au serveur et aux utilisateurs de se mettre d'accord sur algorithme de cryptage pour initer la connexion.| cf. Annexe 2,3,4 |
| Paramétrage des entêtes | La première en-tête mise en place est l'activation de HSTS (HTTP Strict Transport Security) qui permet aux navigateurs de communiquer uniquement via le protocole HTTPS. Dés lors où HSTS est activé le navigateur remplace tous les liens non-sécurisés par des liens sécurisés. La seconde en-tête ajoutée permet de rejeter les éléments avec des types MIME incorrects et d'interdire la détection de contenu pour prémunir les utilisateurs d’une attaque drive-by-download. La dernière en-tête permet d'activer les filtres anti-xss (cross-site scripting). Une attaque xss permet d'injecter du code HTML ou JavaScript dans des variables mal protégées d'un site web. Cette en-tête va permettre de bloquer le rendu de la page si une tentative d'attaque est détectée. | cf. Annexe 5 |
| Lutter contre les attaques de type DDOS | Ce type d'attaque DDOS (Distributed Denial of Service attack) engendre l'indsiponibilté d'un site web en multipliant le nombre  de requête ou de connexion à ce site web. Cette attaque a pour but d'empêcher des utilisateurs légitimes de pouvoir se connecter au site web. | cf. Annexe 6 |

### Conclusion
Nous avons mis en oeuvre de nombreux processus pour tenter de garantir la sécurité de notre serveur en ajoutant des protocoles permettrant de lutter contre diverses attaques. Malgré tout il n'existe pas de sécurité parfaite cependant il est important de surveiller (audit) et de tester régulièrement son serveur afin de s'assurer de sa robustesse. 

### Annexes

## Annexe 1 : Configuration HTTPS (Fichier : /etc/nginx/nginx.conf)
![Configuration HTTPS](./annexes-A/configuration_https.PNG)

Sur cette capture d'écran, on peut constater une variable nommée listen. Cette variable permet d'indiquer au serveur NGINX que le site doit écouter sur le port 443 pour les connexions HTTPS plutôt que sur le port 80 consacré au protocole HTTP.
Nous pouvons également constater la déclaration du certificat par l'autorité tiers (ssl_certificarte) ainsi que la clé secrète générée (ssl_certificate_key) via les commandes openssl.


# Dossier : /etc/nginx/ssl/
Ce dossier contient les différents éléments générés via les commandes openssl.

* Générer une clé secrète : `openssl genrsa -out ./server.key 2048`

* Demande de signature de certificat : `openssl x509 -noout -text -in ./server.crt`

* Création d'un certificat validé par une autorité tiers : `openssl x509 -req -in server.csr -days 365 -CA root-ca-tiw4.cert -CAkey root-ca-tiw4.key -CAcreateserial  -out server.crt`
    
## Annexe 2 : Configuration TLS (Fichier : /etc/nginx/nginx.conf)

![Configuration TLS](./annexes-A/configuration_tls.PNG)

Le déploiement de TLS assure la confidentialité des échanges entre notre application et les utilisateurs en vérifiant qu'aucun utilisateur tiers intercepte ou falsifie un message.

## Annexe 3 : Configuration Suite de Chiffrement (Fichier : /etc/nginx/nginx.conf)
![Configuration Suite de Chiffrement](./annexes-A/configuration_chipers.PNG)

Le but des suites de chiffrements est d'assurer au plus de navigateurs l'accès au site possible sans compromettre la sécurité et la fiabilité du site.

## Annexe 4 : Configuration de la session (Fichier : /etc/nginx/nginx.conf)
![Configuration de la session](./annexes-A/configuration_session.PNG)

## Annexe 5 : Configuration En-têtes (Fichier : /etc/nginx/nginx.conf)
![Configuration En-têtes](./annexes-A/configuration_headers.PNG)

## Annexe 6 : Configuration En-têtes (Fichier : /etc/nginx/nginx.conf)
![Configuration En-têtes](./annexes-A/configuration_ddos.PNG)

La variable 'limit_req' permet de limiter le nombre de requetes maximum par IP et par seconde  
La variable 'limit_conn' permet de limiter le nombre de connexions maximum par IP