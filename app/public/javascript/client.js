const zxcvbn = require("zxcvbn");

const pwElement = $("#pw");
const pw2Element = $("#pw2");
const verifyElement = $(".verify");
const scoreElement = $("#score");
const suggestionsItemlement = $(".suggestions-item");

let password = pwElement[0].value;
let password2 = pw2Element[0].value;
let evaluation = zxcvbn(password);
const scoreEnum = Object.freeze({0 : "Very weak", 1 : "Weak", 2 : "Medium", 3 : "Strong", 4 : "Very Strong"});
let suggestions;

function zxcvbnOnFocus() {
  scoreElement
    .text(scoreEnum[evaluation.score]);
}

function zxcvbnOnChange() {
  password = pwElement[0].value;

  evaluation = zxcvbn(password);
  suggestions = evaluation.feedback.suggestions;
  scoreElement
    .text(scoreEnum[evaluation.score]);

  if (suggestions.length) {
    suggestionsItemlement.removeClass("no-suggestions");
    suggestionsItemlement
      .addClass("suggestions-item")
      .text(suggestions);
  } else {
    suggestionsItemlement.addClass("no-suggestions");
  }
}

function verifyPassword() {
  password = pwElement[0].value;
  password2 = pw2Element[0].value;

  if (password !== password2) {
    console.log("true");
    verifyElement
      .addClass("different");
  } else {
    verifyElement
      .removeClass("different");
  }
}

pwElement.focus(function() {
  zxcvbnOnFocus();
});

pwElement.keyup(function() {
  zxcvbnOnChange();
  verifyPassword();
});

pw2Element.keyup(function() {
  verifyPassword();
});
